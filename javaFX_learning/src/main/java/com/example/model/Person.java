package com.example.model;

import javafx.beans.property.*;

import java.time.LocalDate;

public class Person {
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty street;
    private final StringProperty gender;
    private final ObjectProperty<LocalDate> birthday;

    public Person(){
        firstName=new SimpleStringProperty("");
        lastName=new SimpleStringProperty("");
        street=new SimpleStringProperty("");
        gender=new SimpleStringProperty("");
        birthday=null;
    }


    /**
     * Constructor with some initial data.
     *  @param firstName
     * @param lastName
     * @param gender
     */
    public Person(String firstName, String lastName, String gender) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.gender = new SimpleStringProperty(gender);

        // Some initial dummy data, just for convenient testing.
        this.street = new SimpleStringProperty("Not Set Address");
        this.birthday = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getGender(){
        return gender.get();
    }

    public void setGender(String gender){
        this.gender.set(gender);
    }

    public StringProperty genderProperty(){
        return gender;
    }

    public String getStreet() {
        return street.get();
    }

    public void setStreet(String street) {
        this.street.set(street);
    }

    public StringProperty streetProperty() {
        return street;
    }


    public LocalDate getBirthday() {
        return birthday.get();
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday.set(birthday);
    }

    public ObjectProperty<LocalDate> birthdayProperty() {
        return birthday;
    }
}

