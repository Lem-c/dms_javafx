module com.example.playwindow {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires javafx.graphics;

    opens com.example.playwindow to javafx.fxml;
    opens com.example.control to javafx.fxml;

    exports com.example.playwindow;
    exports com.example.control;

}

